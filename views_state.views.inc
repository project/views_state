<?php

/**
 * @file
 * Provide views state data.
 */

/**
 * Implements hook_views_data().
 */
function views_state_views_data() {
  $data['views_state']['table']['group'] = t('Views state');
  $data['views_state']['table']['join'] = [
    // #global is a special flag which allows a table to appear all the time.
    '#global' => [],
  ];

  $data['views_state']['state'] = [
    'title' => t('Views state data'),
    'help' => t('Add a data from Drupal state.'),
    'field' => [
      'id' => 'views_state_field',
      'click sortable' => FALSE,
    ],
    'area' => [
      'id' => 'views_state_area',
    ],
  ];

  return $data;
}
