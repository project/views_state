<?php

namespace Drupal\views_state\Plugin\views\area;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\State\StateInterface;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Views area state handler.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("views_state_area")
 */
class StateArea extends AreaPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * The key value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected KeyValueFactoryInterface $keyValue;

  /**
   * Constructs a new StateField.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value
   *   The key value store.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, StateInterface $state, KeyValueFactoryInterface $key_value) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->state = $state;
    $this->keyValue = $key_value;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('state'),
      $container->get('keyvalue')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['state'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $options = [];
    foreach ($this->keyValue->get('state')->getAll() as $name => $value) {
      $options[$name] = $name;
    }
    $form['state'] = [
      '#type' => 'select',
      '#title' => $this->t('State'),
      '#default_value' => $this->options['state'],
      '#options' => $options,
      '#description' => $this->t('Select the state to display, complex values will be printed as strings.'),
      '#empty_option' => $this->t('- None -'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $value = $this->state->get($this->options['state'], '');
    if (!is_scalar($value)) {
      $value = print_r($value, TRUE);
    }
    return [
      '#markup' => $value,
    ];
  }

}
